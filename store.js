#!/usr/bin/env node
//above line is placed to use ./store.js without node

const fs = require("fs");
const readline = require("readline");
const path = require("path");
const file = path.join(__dirname, "file.txt");

let key,
  value,
  Obj = {},
  input = process.argv[2];
switch (input) {
  case "add":
    add();
    break;

  case "remove":
    remove();
    break;
  case "list":
    list();
    break;

  case "get":
    get();
    break;

  case "clear":
    clear();
    break;

  case "update":
    update();
    break;

  default:
    console.log("input not found, Please use: (add, remove, list, get, clear or update)");
    break;
}

function add() {
  key = process.argv[3];
    value = process.argv[4];
    if (key && value) {
      fs.readFile(file, (err, data) => {
        if (!err) {
          Obj = JSON.parse(data.toString());
          if (Obj.hasOwnProperty(key)) {
            console.log("This Key already exists, Please enter a new Key");
          } else {
            Obj[key] = value;
            fs.writeFileSync(file, JSON.stringify(Obj));
            console.log("Key and Value have been added!");
          }
        } else console.log("Could not open the file");
      });
    } else console.log("Please enter a Key and a Value");
}

function remove(){
  key = process.argv[3];
    fs.readFile(file, (err, data) => {
      if (!err) {
        Obj = JSON.parse(data.toString());
        if (Obj.hasOwnProperty(key)) {
          delete Obj[key];
          fs.writeFileSync(file, JSON.stringify(Obj));
          console.log("Key and value have been removed!");
        } else console.log("Key is not found, Please enter a valid Key");
      } else console.log("Could not open the file");
    });
}

function list(){
  fs.readFile(file, (err, data) => {
    if (!err) {
      if (data.toString() === "{}") {
        console.log(
          "The file is empty, Please add some objects(Keys/Values)"
        );
      } else console.log(data.toString());
    } else console.log("Could not open the file");
  });
}

function get() {
  key = process.argv[3];
    fs.readFile(file, (err, data) => {
      if (!err) {
        Obj = JSON.parse(data.toString());
        if (Obj.hasOwnProperty(key)) {
          console.log(key + " : " + Obj[key]);
        } else console.log("Error key is not found");
      } else console.log("Could not open the file");
    });
}

function clear() {
  fs.exists(file, exists => {
    if (exists) {
      Obj = {};
      fs.writeFileSync(file, JSON.stringify(Obj));
      console.log("Dictionary have been cleared");
    } else console.log("Could not open the file");
  });
}

function update() {
  key = process.argv[3];
    if (key) {
      fs.readFile(file, (err, data) => {
        if (!err) {
          Obj = JSON.parse(data.toString());
          if (Obj.hasOwnProperty(key)) {
            const rl = readline.createInterface({
              input: process.stdin,
              output: process.stdout
            });
            rl.question(`Please add new value to key (${key})`, answer => {
              Obj[key] = answer;
              fs.writeFileSync(file, JSON.stringify(Obj));
              console.log("Updated the value of the key!");
              rl.close();
            });
          } else console.log("Key Could not be found!");
        } else console.log("Could not open the file");
      });
    } else {
      console.log("Please enter a Key");
    }
}